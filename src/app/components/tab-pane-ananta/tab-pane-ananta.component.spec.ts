import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabPaneAnantaComponent } from './tab-pane-ananta.component';

describe('TabPaneAnantaComponent', () => {
  let component: TabPaneAnantaComponent;
  let fixture: ComponentFixture<TabPaneAnantaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabPaneAnantaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabPaneAnantaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
