import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-tab-pane-sublime',
  templateUrl: './tab-pane-sublime.component.html',
  styleUrls: ['./tab-pane-sublime.component.css']
})
export class TabPaneSublimeComponent implements OnInit {

  selected_date:String;
  subject_array = [];

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { 

    this.dataService.currentDateValue.subscribe(date => {
      
      this.selected_date = date;
      
      this.authService.getAllFirstEvents('sgt',this.selected_date).subscribe(data => {

        this.subject_array = data;
  
      });
    
    });

  }

  ngOnInit() {

  }


}
