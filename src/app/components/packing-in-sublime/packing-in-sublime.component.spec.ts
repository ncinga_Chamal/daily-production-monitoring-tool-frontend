import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackingInSublimeComponent } from './packing-in-sublime.component';

describe('PackingInSublimeComponent', () => {
  let component: PackingInSublimeComponent;
  let fixture: ComponentFixture<PackingInSublimeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackingInSublimeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackingInSublimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
