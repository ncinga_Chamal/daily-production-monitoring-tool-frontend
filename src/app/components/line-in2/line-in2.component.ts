import { Component, OnInit, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Station } from '../../classes/station';

@Component({
  selector: 'app-line-in2',
  templateUrl: './line-in2.component.html',
  styleUrls: ['./line-in2.component.css']
})
export class LineIn2Component implements OnInit {

  selected_date:String;
  selectedStation: Station;

  @Input() subject_array;

  line_in_2:Station[] = [
    {
        at:"line_in2",
        sub: "floor1-sewing-section1-line1",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line1",
        dis_name: "TEAM01",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section1-line2",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line2",
        dis_name: "TEAM02",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section1-line3",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line3",
        dis_name: "TEAM03",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section1-line4",
        sub_ex: "ant-aal-aepz-floor1-sewing-section1-line4",
        dis_name: "TEAM04",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section2-line5",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line5",
        dis_name: "TEAM05",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section2-line6",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line6",
        dis_name: "TEAM06",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section2-line7",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line7",
        dis_name: "TEAM07",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section2-line8",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line8",
        dis_name: "TEAM08",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    },
    {
        at:"line_in2",
        sub: "floor1-sewing-section2-line9",
        sub_ex: "ant-aal-aepz-floor1-sewing-section2-line9",
        dis_name: "TEAM09",
        hsk: "assembly_point1",
        f_event:"red-palatte"
    }
  ]; 


  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) { }

  ngOnInit() {

  }

  ngOnChanges() {

    for(var i=0;i<this.line_in_2.length;i++){
      
      this.line_in_2[i].f_event = "red-palatte";

    }

    if(this.subject_array.length != 0){

      for(var i=0;i<this.line_in_2.length;i++){

        for(var j=0;j<this.subject_array.length;j++){

          if(this.subject_array[j].at == 'line_in2' && this.subject_array[j].sub == this.line_in_2[i].sub_ex && this.subject_array[j].hsk == this.line_in_2[i].hsk){

            this.line_in_2[i].f_event = "green-palatte";

          }
        
        }

      }

    }

  }

  onSelectStation(station: Station): void {
    this.selectedStation = station;
    this.dataService.changeStation(this.selectedStation);
  }

}
