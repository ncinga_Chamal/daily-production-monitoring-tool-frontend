import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LineIn2Component } from './line-in2.component';

describe('LineIn2Component', () => {
  let component: LineIn2Component;
  let fixture: ComponentFixture<LineIn2Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LineIn2Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LineIn2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
