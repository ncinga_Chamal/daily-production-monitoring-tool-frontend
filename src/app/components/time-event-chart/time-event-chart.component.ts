import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { DataService } from '../../services/data.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-time-event-chart',
  templateUrl: './time-event-chart.component.html',
  styleUrls: ['./time-event-chart.component.css']
})
export class TimeEventChartComponent implements OnInit {

  chart = [];
  selected_index:String;
  selected_subject:String;
  progress:String;
  data_point_keys = [];
  data_point_values = [];

  constructor(
    private authService:AuthService,
    private dataService:DataService
  ) {

    this.dataService.currentIndexSelectionValue.subscribe(data => {
      this.selected_index = data;
    });

    this.dataService.currentSubjectSelectionValue.subscribe(data => {
      this.selected_subject = data;
      this.onClickChangeChart();
    });

  }

  ngOnInit() {
  }

  onClickChangeChart(){

    this.authService.getTimeEvent(this.selected_index,this.selected_subject).subscribe(data => {

      this.data_point_keys.length = 0;
      this.data_point_values.length = 0;

      for (var i = 0; i < data.aggregations[2].buckets.length; i++) {
        this.data_point_keys.push(data.aggregations[2].buckets[i].key);
      }

      for (var i = 0; i < data.aggregations[2].buckets.length; i++) {
        this.data_point_values.push(data.aggregations[2].buckets[i][1].value);
      }

      let timetag_dates = [];

      this.data_point_keys.forEach((res) => {
        let jsdate = new Date(res);
        timetag_dates.push(jsdate.toLocaleTimeString('en', { year: 'numeric', month: 'short', day: 'numeric' }))
      });

      this.chart.length = 0;

      this.chart = new Chart('canvas', {
        responsive: true,
        type: 'line',
        data: {
          labels: timetag_dates,
          datasets: [
            { 
              data: this.data_point_values,
              borderColor: "#1976d2",
              fill: false
            }
          ]
        },
        options: {
          responsive: true,
          legend: {
            display: false,
          },
          scales: {
            xAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'time_tags.datetime per minute',
                fontSize:18,
                fontColor:'#000000'
              }
            }],
            yAxes: [{
              display: true,
              scaleLabel: {
                display: true,
                labelString: 'Sum of points.attrib_val',
                fontSize:18,
                fontColor:'#000000'
              }
            }],
          }
        }
      });

    });

  }

}
