import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.css']
})
export class TopbarComponent implements OnInit {

  current_date:String;

  constructor(
    private dataService:DataService
  ) { }

  ngOnInit() {
    this.current_date = this.dataService.getCurrentDateOtherFormat();
  }

  setEventDate(date){
    this.dataService.formatDateAndChangeSource(date);
  }

}
