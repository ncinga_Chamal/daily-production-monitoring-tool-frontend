import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Station } from '../classes/station';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  
  selectedStation:Station;

  private factorySelectionSource = new BehaviorSubject('ananta');
  private dateSource = new BehaviorSubject(this.getCurrentDate());
  private stationSelectionSource = new BehaviorSubject(this.selectedStation);
  private indexSelectionSource = new BehaviorSubject('*ops-time*');
  private subjectSelectionSource = new BehaviorSubject('*');  

  currentFactoryValue = this.factorySelectionSource.asObservable();
  currentDateValue = this.dateSource.asObservable();
  currentStationSelectionValue = this.stationSelectionSource.asObservable();
  currentIndexSelectionValue = this.indexSelectionSource.asObservable();
  currentSubjectSelectionValue = this.subjectSelectionSource.asObservable();

  today: Date;
  todays: String;
  dd: Number;
  dds: String;
  mms: String;
  mm: Number;
  yyyy: Number;
  zero: String;

  constructor() { }

  changeFactoryPane(factory){
    this.factorySelectionSource.next(factory);
  }

  getCurrentDate(){
    
    this.today = new Date();
    this.dd = this.today.getDate();
    this.mm = this.today.getMonth() + 1;
    this.yyyy = this.today.getFullYear();

    if(this.dd < 10) {
      this.dds = '0' + this.dd;
    } else {
      this.dds = this.dd.toString();
    } 
  
    if(this.mm < 10) {
      this.mms = '0'+ this.mm;
    } else {
      this.mms = this.mm.toString();
    }

    this.todays = this.yyyy + '.' + this.mms + '.' + this.dds;
    return this.todays;
  }

  getCurrentDateOtherFormat(){
    
    this.today = new Date();
    this.dd = this.today.getDate();
    this.mm = this.today.getMonth() + 1;
    this.yyyy = this.today.getFullYear();

    if(this.dd < 10) {
      this.dds = '0' + this.dd;
    } else {
      this.dds = this.dd.toString();
    } 
  
    if(this.mm < 10) {
      this.mms = '0'+ this.mm;
    } else {
      this.mms = this.mm.toString();
    }

    this.todays = this.mms + '/' + this.dds + '/' + this.yyyy;
    return this.todays;
  }

  formatDateAndChangeSource(date){
    var splitted_date = date.split("-", 3);
    var formatted_date = splitted_date[0] + '.' + splitted_date[1] + '.' + splitted_date[2];
    this.dateSource.next(formatted_date);
  }

  changeStation(station:Station){
    this.stationSelectionSource.next(station);
  }

  changeIndex(selected_index){
    this.indexSelectionSource.next(selected_index);
  }

  changeSubject(selected_subject){
    this.subjectSelectionSource.next(selected_subject);
  }

}
