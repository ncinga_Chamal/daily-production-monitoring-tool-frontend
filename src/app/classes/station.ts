import { Line } from './line';

export class Station {
    at:String;
    sub:String;
    sub_ex:String
    dis_name:String;
    hsk:String;
    f_event:String;
}